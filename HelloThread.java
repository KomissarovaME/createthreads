package current;

/**
 * Создание подкласса Thread
 *
 * Класс Thread сам реализует интерфейс Runnable
 * метод run() ничего не делает
 * Подкласс может обеспечить собственную реализацию метода run()
 */
public class HelloThread extends Thread {

    public static void main(String[] args) {
        for (int i=0; i<10; i++) {
            (new HelloThread()).start();
        }
        System.out.println("Hello from main thread!");
    }

    public void run() {
        Thread thread = Thread.currentThread();
        System.out.println("Имя потока: " + getName() + " (" + getPriority() + ")");
    }
}
