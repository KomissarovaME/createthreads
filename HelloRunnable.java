package current;

/**
 * Реализация интерфейса Runnable
 *
 * Интерфейс Runnable определяет один метод run()
 * преднозначенный для размещения кода исполняемого в потоке
 * Runnable-объект передаётся в конструктор Thread
 * и с помощью метода start() поток запускается
 */

public class HelloRunnable implements Runnable {
    public void run() {
        System.out.println("Hello from a thread!");
    }

    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();
        //(new Thread(new HelloRunnable())).run(); - будет выполненное последовательное выполнение
        System.out.println("Hello from a main thread!");
    }
}
// Если нужно будет вновь запустить run() придётся создать новый объект и вызвать его.
